#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo Exporting GOPATH=${DIR}
export GOPATH=${DIR}
export GOOS=linux #You will want to change this to the target environment

echo Checking Dependencies...
$(go get gopkg.in/mgo.v2)

echo Building orbitrage data services ${GOOS}...
$(go build -o ./bin/orbitrage ./src/orbitrage.go)

echo Building orbitrage analytical services ${GOOS}...
$(go build -o ./bin/orbitrageAnalytics ./src/orbitrageAnalytics.go)

echo Done