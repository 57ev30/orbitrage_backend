# orbitrage - web / data / analytics services - v0.0.6 #

This is the orbitrage backend data service. It is a rest-like interface that serves as a web server and as a 
data server for the orbitrage web front-end and scrapers. There is a separate analytics provider that is not fully implemented yet...

## Dependencies ##

* go sdk v1.8
* mgo v2

## How to build and run ##

* cd to project root directory
* run build.sh
* run ./bin/orbitrage to launch the web / data server
* run ./bin/orbitrageAnalytics to launch the analytics server

## Notes ##

* When running on a *nix platform you will need to run the services under a user with the needed network privileges or allow binding to ports lower than 1024 by running the below code in a terminal

```sudo setcap CAP_NET_BIND_SERVICE=+eip ./path/to/binary```

## Configuration ##

#### Data / Web Services ####

Below is an example ./config file for the orbitrage data service. This file path is hardcoded into the application.

```
{
  "ApiKey" : "8341b42975315735718fdfa1e3203463890a2072260eb191836e7b41e85e31f9ec5abc4b8c98d01ac3371302f49692af4c51c5751d954b7571d56212ac75c0069a03",
  "DataWebPath":"/ds",
  "LogPath" : "/orbitrage.log",
  "WebPath" : "/www",
  "WebAddr" : ":443",
  "TlsCert" : "/cert/server.pem",
  "TlsKey" : "/cert/server.key",
  "MongoServer" : "host:27017",
  "MongoDatabase" : "orbitrage",
  "MongoUser" : "username",
  "MongoPassword" : "password"
}
```

#### Analytics Services ####

Below is an example ./configAnalytics file for the orbitrage data analytics service. This file path is hardcoded into the application.

```
{
  "ApiKey" : "8341b42975315735718fdfa1e3203463890a2072260eb191836e7b41e85e31f9ec5abc4b8c98d01ac3371302f49692af4c51c5751d954b7571d56212ac75c0069a03",
  "LogPath" : "/orbitrageAnalytics.log",
  "DataServer":"https://host/ds",
  "CollectionIn":"ticker",
  "CollectionOut":"analytics",
  "AnalyseInterval":1
}
```

## Usage ##

There are three calls exposed via the data service interface as described below:

#### Login ####

In order to login into the system you must perform a login request. If successful a token will be returned that must be 
included in all subsequent requests made to the server contained within the "x-api-key" header.

```https://host/ds?a=login&u=user&p=password```

Success:

```{"t":"0da467b7c0af757a60b288bdc37d653e069c6b1b0ab2b25b2e0c8994f9c801d72f9dd394e5dec16f2ee35176c199a419ec4e59008bdd24c956bf4c185bbb2b1b"}```

Failed:

```{"Error":"Login Failed!"}```

#### Get ####

To retrieve data you can use the get action. Just specify the collection and a valid mongoDB aggregation pipeline. 
Refer to [MongoDb Documentation](https://docs.mongodb.com/manual/reference/)for more information

The below pipeline will match data updated after the specified date, then get the total volume and average bid per country 
sorted descending by volume.

```https://host/ds?a=get&c=ticker&q=[{"$match": {"Updated": {"$gte": ISODate("2017-06-25T12:11:30.308Z")}}}, {"$group": {"_id":"$Country", "totalVol":{"$sum":"$Volume"}, "avgBid":{"$avg":"$Bid"}}}, {"$sort":{"totalVol":-1}}]```

Success:

```[{"_id":"usa","avgBid":851.6469846490536,"totalVol":191569344.03239334},{"_id":"mex","avgBid":954.5581748338064,"totalVol":131941994.25378138},{"_id":"jpn","avgBid":2659.7075743589717,"totalVol":27582001.003376964},{"_id":"rus","avgBid":2701.4538918946405,"totalVol":17685754.074748967},{"_id":"hkg","avgBid":2529.51272189349,"totalVol":5722223.4809833225},{"_id":"can","avgBid":1477.599978223489,"totalVol":1898746.8791739994},{"_id":"dnk","avgBid":2592.5759680911683,"totalVol":334091.70911188},{"_id":"zaf","avgBid":2808.6323964661024,"totalVol":111019.90430099971},{"_id":"pol","avgBid":2525.9129512893965,"totalVol":39404.95},{"_id":"chn","avgBid":2856.3846535612474,"totalVol":13827},{"_id":"gbr","avgBid":2732.449667714283,"totalVol":0}]```

Error:

```{"Error":"Failed while marshalling query!"}```
```{"Error":"Failed while running query against database!"}```
```{"Error":"Failed while marshalling query results!"}```

#### Put ####

Data may be added by specifying the put action, supplying the collection name and finally providing the json document 
you wish to add to the collection.

```https://host/ds?a=put&c=collectionName&q={key:value}```

Success:

```{"Success":"Document added to collection"}```

Error:

```{"Error":"User Not Authenticated!"}```

```{"Error":"Failed to add document!"}```

```{"Error":"Not adding blank documents to the collection! ERROR: Invalid document specified!"}"```

```{"Error":"Failed to marshal json document to interface!"}```

```{"Error":"Failed to add document {key:value} to collection collectionName!"}```

## Analytics Services ##

The orbitrage analytics server is a stand-alone server that will use the orbitrage data services to retrieve data and then 
run configured mongoDB aggregation pipelines against an input collection specified in configuration. The result will be put 
to orbitrage data services for consumption. Unfortunately the analytics server is not fully implemented yet... So watch this space!