package main

import (
	"sync"
	"log"
	"os"
	"os/signal"
	"syscall"
	"runtime"
	"strconv"
	"strings"
	"fmt"
	"path"
	"io/ioutil"
	"encoding/json"
	"net/http"
	"time"
	"bytes"
	"errors"
)

//////////////////////////////////
// Types

type logEntryT struct {
	Level	int		`json:"Level"`
	Message	string	`json:"Message"`
}

type configT struct {
	ApiKey 			string 	`json:"ApiKey"`
	LogPath     	string 	`json:"LogPath"`
	DataServer		string 	`json:"DataServer"`
	CollectionIn	string	`json:"CollectionIn"`
	CollectionOut	string	`json:"CollectionOut"`
	AnalyseInterval int 	`json:"AnalyseInterval"`
}

//////////////////////////////////
// Constants

const (
	// Application
	APP_NAME_VERSION = "orbitrage - Analytics Services - v0.0.1"

	// Logging
	LOG_LVL_SECURITY = 0
	LOG_LVL_ERROR 	 = 1
	LOG_LVL_MSG 	 = 2
)

//////////////////////////////////
// Variables

var (
	confPath string = "./configAnalytics"
	config configT = configT{}

	isTimeToQuit bool = false
	wgGoprocs sync.WaitGroup

	logger *log.Logger = nil
	logBufferSize int = 100
	logChannel chan logEntryT = make(chan logEntryT, logBufferSize)

	webClient *http.Client = &http.Client{}
)


//////////////////////////////////
// Main

func main() {
	if strings.ToLower(runtime.GOOS) != "windows" {
		fmt.Print("\033[2J")
	}

	err := initialise()

	if err != nil {
		logErr("Initialisation failed!", err)
		shutdown()
		return
	}

	signals := make(chan os.Signal)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM, syscall.SIGABRT)
	<- signals

	shutdown()
}

// Initialises the application
func initialise() error {
	if err := loadConfig(); err != nil {
		println("Failed to load config! ERROR: " + err.Error())
		return err
	}

	if len(config.ApiKey) <= 0 {
		err := errors.New("Invalid api key specified in configuration!")
		println("Configuration validation failed! ERROR: " + err.Error())
		return err
	}

	if len(config.DataServer) <= 0 {
		err := errors.New("Invalid data server specified in configuration!")
		println("Failed to load config! ERROR: " + err.Error())
		return err
	}

	if len(config.CollectionIn) <= 0 {
		err := errors.New("Invalid input collection specified in configuration!")
		println("Configuration validation failed! ERROR: " + err.Error())
		return err
	}

	if len(config.CollectionOut) <= 0 {
		err := errors.New("Invalid output collection specified in configuration!")
		println("Configuration validation failed! ERROR: " + err.Error())
		return err
	}

	if err := resolvePaths(); err != nil {
		println("Failed to resolve paths! ERROR: " + err.Error())
		return err
	}

	go logProcess()

	hostname, err := os.Hostname()

	if err != nil {
		hostname = "unknown"
	}

	logMsg(APP_NAME_VERSION)
	logMsg("Running on " + hostname + " " + runtime.GOARCH + " " + runtime.GOOS + " with " + strconv.Itoa(runtime.NumCPU()) + " processors")
	logMsg("Using " + runtime.Version() + " runtime")

	go analyticsServerProcess()

	return nil
}

// Shuts the application down safely
func shutdown() {
	isTimeToQuit = true

	logMsg("Shutting down...")
	logMsg("Waiting for goprocs to finish...")
	wgGoprocs.Wait()

	println("Shut down")
}

//////////////////////////////////
// Utility

// Loads configuration from file
func loadConfig() error {
	if _, err := os.Stat(confPath); !os.IsNotExist(err) {
		confFile, err := os.Open(confPath)
		defer confFile.Close()

		if err != nil {
			return err
		}

		b, err := ioutil.ReadAll(confFile)

		if err != nil {
			return err
		}

		if err := json.Unmarshal(b, &config); err != nil {
			return err
		}
	}

	return nil
}

// Adds an error message to the log channel
func logErr(msg string, err error) {
	if err != nil {
		msg = "ERROR: " + msg + " DETAILS: " + err.Error()
	} else {
		msg = "ERROR: " + msg
	}

	logChannel <- logEntryT {
		Level: LOG_LVL_ERROR,
		Message: msg,
	}
}

// Adds a general message to the log channel
func logMsg(msg string) {
	logChannel <- logEntryT {
		Level: LOG_LVL_MSG,
		Message: msg,
	}
}

// Processes the log channel
func logProcess() {
	wgGoprocs.Add(1)
	defer wgGoprocs.Done()

	logFile, err := os.OpenFile(config.LogPath, os.O_APPEND | os.O_CREATE | os.O_WRONLY, 0666)
	defer logFile.Close()

	if err != nil {
		println("Failed to initialise logger... DETAILS : ", err.Error())
		return
	}

	logger = log.New(logFile, "", log.Ldate | log.Lmicroseconds)

	for !isTimeToQuit {
		logEntry := <-logChannel

		println(logEntry.Message)
		logger.Println(logEntry.Message)
	}

	println("Log channel processing stopped")
}

// Determine the executable path and set application relative paths
func resolvePaths() error {
	executable, err := os.Executable()

	if err != nil {
		return err
	}

	executablePath := path.Dir(executable)
	config.LogPath = executablePath + config.LogPath

	return nil
}


//////////////////////////////////
// Analytics

func analyticsServerProcess() {
	wgGoprocs.Add(1)
	defer wgGoprocs.Done()

	logMsg("Analytics server processing started...")

	for !isTimeToQuit {
		result, err := getAnalyticsQueryResult()

		if err != nil {
			time.Sleep(1 * time.Minute)
			continue
		}

		err = putAnalyticsQueryResult(result)

		if err != nil {
			time.Sleep(1 * time.Minute)
			continue
		}

		time.Sleep(time.Duration(config.AnalyseInterval) * time.Second)
	}
}

func getLastUpdated() (string, error) {
	val := "2017-06-00T00:00:00.000Z"
	lastUpdated := `ISODate("` + val + `")`
	queryGet := config.DataServer + `?a=get&c=analyticsRun&q=["{$match:{}}"]`
	queryPut := config.DataServer + `?a=put&c=analyticsRun&q={"LastRun":"` + val + `"}`

	if resp, err := http.Get(queryGet); err != nil {
		if resp.StatusCode != 200 {
			logErr("Failed to get last updated analytics run date, response code was not 200!", err)
			return lastUpdated, err
		}

		body, err := ioutil.ReadAll(resp.Body)

		if err != nil {
			logErr("Failed to read last updated analytics response body!", err)
		}

		resp.Body.Close()
		runMap := make(map[string]string)

		if err := json.Unmarshal(body, runMap); err != nil {
			if lastVal, ok := runMap["foo"]; ok {
				val = lastVal
			}
		}

		lastUpdated = `ISODate("` + val + `")`
	} else {
		if _, err := http.Get(queryPut); err != nil {
			return "", err
		}
	}

	return lastUpdated, nil
}

func getAnalyticsQuery() (string, error) {
	// TODO: This should be reading queries from a persisted store

	lastUpdated, err := getLastUpdated()

	if err != nil {
		return "", err
	}

	match := `{"$match":{"Updated": {"$gt":` + lastUpdated + `}}}`
	analyticsQuery := `{"$group": {"_id":{}}}`
	query := config.DataServer + "?a=get&c=" + config.CollectionIn + "&q=[" + match + ", " + analyticsQuery + "]"

	return query, nil
}

func getAnalyticsQueryResult() ([]byte, error) {
	query, err := getAnalyticsQuery()

	if err != nil {
		logErr("Failed to get a analytics query!", err)
		return nil, err
	}

	req, err := http.NewRequest(http.MethodGet, query, nil)

	if err != nil {
		logErr("Failed to create the web client request!", err)
		return nil, err
	}

	req.Header.Add("x-api-key", config.ApiKey)
	resp, err := webClient.Do(req)

	if err != nil {
		logErr("Failed to run analytics query request!", err)
		return nil, err
	}

	if resp.StatusCode != 200 {
		logErr("Failed to run analytics query request, response code was not 200!", err)
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		resp.Body.Close()
		logErr("Failed to read analytics query response body!", err)
		return nil, err
	}

	resp.Body.Close()

	return body, nil
}

func putAnalyticsQueryResult(result []byte) error {
	if len(result) <= 0 {
		return nil
	}

	var buffer *bytes.Buffer
	json.HTMLEscape(buffer, result)

	query := config.DataServer + "?a=put&c=" + config.CollectionOut + "&q=" + string(buffer.Bytes())

	req, err := http.NewRequest(http.MethodGet, query, nil)

	if err != nil {
		logErr("Failed to create the web client request!", err)
		return err
	}

	req.Header.Add("x-api-key", config.ApiKey)
	resp, err := webClient.Do(req)

	if resp.StatusCode != 200 {
		logErr("Failed to put analytics result, response code was not 200!", err)
		return err
	}

	return nil
}