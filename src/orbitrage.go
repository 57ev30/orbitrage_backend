package main

//////////////////////////////////
// Imports

import (
	"io"
	"os"
	"os/signal"
	"syscall"
	"runtime"
	"sync"
	"fmt"
	"path"
	"log"
	"time"
	"strconv"
	"strings"
	"net/http"
	"net/url"
	"crypto/rand"
	"crypto/tls"
	"crypto/sha512"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"encoding/json"
	"io/ioutil"
)


//////////////////////////////////
// Types

type logEntryT struct {
	Level	int		`json:"Level"`
	Message	string	`json:"Message"`
}

type configT struct {
	ApiKey string `json:"ApiKey"`

	DataWebPath string `json:"DataWebPath"`
	LogPath string `json:"LogPath"`
	WebPath string `json:"WebPath"`

	WebAddr string `json:"WebAddr"`
	TlsCert string `json:"TlsCert"`
	TlsKey string `json:"TlsKey"`

	MongoServer string `json:"MongoServer"`
	MongoDatabase string `json:"MongoDatabase"`
	MongoUser string `json:"MongoUser"`
	MongoPassword string `json:"MongoPassword"`
}

type sessionT struct {
	T      string `json:"T"`
	User   string `json:"User"`
	Addr   string `json:"Addr"`
	Expire time.Time `json:"Expire"`
}


//////////////////////////////////
// Constants

const (
	// Application
	APP_NAME_VERSION = "orbitrage - Web and Data Services - v0.0.6"

	// Logging
	LOG_LVL_SECURITY = 0
	LOG_LVL_ERROR 	= 1
	LOG_LVL_MSG 	= 2
)


//////////////////////////////////
// Variables

var (
	confPath string = "./config"
	config configT = configT{}

	isTimeToQuit bool = false
	wgGoprocs sync.WaitGroup

	webServer *http.Server
	sessions map[string]sessionT = make(map[string]sessionT)
	mongoSession *mgo.Session = nil

	logger *log.Logger = nil
	logBufferSize int = 100
	logChannel chan logEntryT = make(chan logEntryT, logBufferSize)
)


//////////////////////////////////
// Main

func main() {
	if strings.ToLower(runtime.GOOS) != "windows" {
		fmt.Print("\033[2J")
	}

	err := initialise()

	if err != nil {
		logErr("Initialisation failed!", err)
		shutdown()
		return
	}

	signals := make(chan os.Signal)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM, syscall.SIGABRT)
	<- signals

	shutdown()
}

// Initialises the application
func initialise() error {
	if err := loadConfig(); err != nil {
		println("Failed to load config! ERROR: " + err.Error())
		return err
	}

	setApiKey()

	if err := resolvePaths(); err != nil {
		println("Failed to resolve paths! ERROR: " + err.Error())
		return err
	}

	go logProcess()

	hostname, err := os.Hostname()

	if err != nil {
		hostname = "unknown"
	}

	logMsg(APP_NAME_VERSION)
	logMsg("Running on " + hostname + " " + runtime.GOARCH + " " + runtime.GOOS + " with " + strconv.Itoa(runtime.NumCPU()) + " processors")
	logMsg("Using " + runtime.Version() + " runtime")

	if err := initialiseMongo(); err != nil {
		return err
	}

	go startWebServer()

	return nil
}

// Initialises a mongoDB session; failure is fatal
func initialiseMongo() error {
	logMsg("Initialising mongoDB session...")

	info := &mgo.DialInfo{
		Addrs:    []string{config.MongoServer},
		Timeout:  60 * time.Second,
		Database: config.MongoDatabase,
		Username: config.MongoUser,
		Password: config.MongoPassword,
	}

	session, err := mgo.DialWithInfo(info)

	if err != nil {
		logErr("Failed to dial mongoDB!", err)
		return err
	}

	session.SetMode(mgo.Monotonic, true)

	mongoSession = session
	logMsg("mongoDB session initialised successfully.")

	return nil
}

// Shuts the application down safely
func shutdown() {
	isTimeToQuit = true

	logMsg("Shutting down...")

	// Shutdown mongo session
	if mongoSession != nil {
		logSec("Logging out of mongoDB and shutting down session...", nil)

		mongoSession.LogoutAll()
		mongoSession.Close()
	}

	logMsg("Waiting for goprocs to finish...")
	wgGoprocs.Wait()

	println("Shut down")
}


//////////////////////////////////
// Utility

// Loads configuration from file
func loadConfig() error {
	if _, err := os.Stat(confPath); !os.IsNotExist(err) {
		confFile, err := os.Open(confPath)
		defer confFile.Close()

		if err != nil {
			return err
		}

		b, err := ioutil.ReadAll(confFile)

		if err != nil {
			return err
		}

		if err := json.Unmarshal(b, &config); err != nil {
			return err
		}
	}

	return nil
}

// Adds a security message to the log channel
func logSec(msg string, err error) {
	if err != nil {
		msg = "SECURITY# " + msg + " DETAILS: " + err.Error()
	} else {
		msg = "SECURITY# " + msg
	}

	logChannel <- logEntryT {
		Level: LOG_LVL_SECURITY,
		Message: msg,
	}
}

// Adds an error message to the log channel
func logErr(msg string, err error) {
	if err != nil {
		msg = "ERROR: " + msg + " DETAILS: " + err.Error()
	} else {
		msg = "ERROR: " + msg
	}

	logChannel <- logEntryT {
		Level: LOG_LVL_ERROR,
		Message: msg,
	}
}

// Adds a general message to the log channel
func logMsg(msg string) {
	logChannel <- logEntryT {
		Level: LOG_LVL_MSG,
		Message: msg,
	}
}

// Processes the log channel
func logProcess() {
	wgGoprocs.Add(1)
	defer wgGoprocs.Done()

	logFile, err := os.OpenFile(config.LogPath, os.O_APPEND | os.O_CREATE | os.O_WRONLY, 0666)
	defer logFile.Close()

	if err != nil {
		println("Failed to initialise logger... DETAILS : ", err.Error())
		return
	}

	logger = log.New(logFile, "", log.Ldate | log.Lmicroseconds)

	for !isTimeToQuit {
		logEntry := <-logChannel

		println(logEntry.Message)
		logger.Println(logEntry.Message)
	}

	println("Log channel processing stopped")
}

// Determine the executable path and set application relative paths
func resolvePaths() error {
	executable, err := os.Executable()

	if err != nil {
		return err
	}

	executablePath := path.Dir(executable)
	config.LogPath = executablePath + config.LogPath
	config.WebPath = executablePath + config.WebPath
	config.TlsCert = executablePath + config.TlsCert
	config.TlsKey = executablePath + config.TlsKey

	return nil
}


//////////////////////////////////
// Security

// Authenticates user and password against mongoDB backend and returns a token
func login(user, password, addr string) (string, error) {
	session := sessionT {
		T:      newToken(),
		User:   user,
		Addr:   addr,
		Expire: time.Now().AddDate(0, 0, 1),
	}

	info := &mgo.DialInfo{
		Addrs:    []string{config.MongoServer},
		Timeout:  60 * time.Second,
		Database: config.MongoDatabase,
		Username: session.User,
		Password: password,
	}

	authSession, err := mgo.DialWithInfo(info)

	if err != nil {
		logSec("Failed to login user!", err)
		return "", err
	}

	authSession.LogoutAll()
	sessions[session.T] = session
	logSec("LOGIN SUCCESS - " + user + " - " + addr, nil)

	return session.T, nil
}

// Generates a new random token
func newToken() string {
	token := make([]byte, 32)
	_, err := io.ReadFull(rand.Reader, token)

	if err != nil {
		logSec("Failed to generate token!", err)
		return ""
	}

	return hash(string(token))
}

// Creates a SHA512 hash
func hash(in string) string {
	hash := sha512.New()
	hash.Write([]byte(in))
	return fmt.Sprintf("%x", hash.Sum(nil))
}

// Creates a new api session using the ApiKey specified in config
func setApiKey() {
	session := sessionT {
		T:      config.ApiKey,
		User:   "API",
		Addr:   "API",
		Expire: time.Now().AddDate(99, 0, 0),
	}

	sessions[session.T] = session
}


//////////////////////////////////
// Web Server

// Starts up the web interface
func startWebServer() error {
	logMsg("Starting Web Interface on " + config.WebAddr)

	http.HandleFunc(config.DataWebPath, dataHandler)
	http.HandleFunc("/", webHandler)

	webServer = &http.Server{
		Addr:           config.WebAddr,
		Handler:        nil,
		ReadTimeout:    60 * time.Second,
		WriteTimeout:   60 * time.Second,
		MaxHeaderBytes: 1 << 20,
		TLSConfig: &tls.Config{
			MinVersion:               tls.VersionTLS12,
			CurvePreferences:         []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256 },
			PreferServerCipherSuites: true,
			CipherSuites: []uint16{
				tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
				tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
			},
		},
	}

	// Start the web server using the configured certificate and key
	err := webServer.ListenAndServeTLS(config.TlsCert, config.TlsKey)

	if err != nil {
		logErr("Fatal error occurred while listening for incoming web server requests!", err)
		return err
	}

	return nil
}

// Validates incoming web requests, returns a reason message if request is invalid
func validateIncomingRequest(r *http.Request) string {
	t := r.Header.Get("x-api-key")

	if len(t) == 0 {
		return "User Not Authenticated!"
	}

	if _, ok := sessions[t]; !ok {
		return "User Not Authenticated!"
	}

	return ""
}

// Handles incoming web file requests
func webHandler(w http.ResponseWriter, r *http.Request) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered from web handler panic! ", r)
		}
	}()

	requestPath := r.URL.EscapedPath()
	filePath := config.WebPath + requestPath
	http.ServeFile(w, r, filePath)
}

// Handles incoming web data requests
func dataHandler(w http.ResponseWriter, r *http.Request) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered from data handler panic! ", r)
		}
	}()

	requestPath := r.URL.EscapedPath()
	query := r.URL.Query()
	action := query.Get("a")

	w.Header().Add("Cache-Control", "no-cache, no-store")
	w.Header().Add("Pragma", "no-cache")
	w.Header().Add("Expires", "0")
	w.Header().Add("Upgrade-Insecure-Requests", "1")
	w.Header().Add("Strict-Transport-Security", "max-age=63072000; includeSubDomains")
	w.Header().Add("X-Frame-Options", "deny")
	w.Header().Add("X-XSS-Protection", "1; mode=block")
	w.Header().Add("X-Content-Type-Options", "nosniff")
	w.Header().Add("Access-Control-Allow-Origin", "*")

	if strings.ToLower(action) == "login" {
		handleLogin(w, r)
		return
	}

	if reason := validateIncomingRequest(r); len(reason) > 0 {
		logSec("BLOCKED Request | Remote Address: " + r.RemoteAddr + "; Origin: " + r.Header.Get("origin") + "; Referrer: " + r.Referer() + "; Path: " + requestPath + "; Reason: " + reason, nil)
		fmt.Fprintf(w, "{\"Error\":\"" + reason + "\"}")
		return
	}

	switch strings.ToLower(action) {
	case "get":
		handleGet(w, query)
		return
	case "put":
		handlePut(w, query)
		return
	default:
		logMsg("Action " + action + " is not supported!")
		fmt.Print(w, "{\"Error\":\"Invalid Request Action " + action + "\"}")
		return
	}
}

// Handles login requests
// Format : http://host:port/?a=login&u=user&p=password
func handleLogin(w http.ResponseWriter, r *http.Request){
	defer func() {
		if r := recover(); r != nil {
			logSec("Recovered from handleLogin panic!", nil)
		}
	}()

	// Don't process browser favicon requests
	if strings.Contains(r.URL.EscapedPath(), "favicon.ico") {
		return
	}

	query := r.URL.Query()
	user := query.Get("u")
	password := query.Get("p")

	if t, err := login(user, password, r.RemoteAddr); err == nil {
		fmt.Fprint(w, "{\"t\":\"" + t + "\"}")
		return
	}

	fmt.Fprint(w, "{\"Error\":\"Login Failed!\"}")
}

// Handles get requests
// Format : http://host:port/?a=get&c=collectionName&q={"key":"value"}
func handleGet(w http.ResponseWriter, q url.Values) {
	defer func() {
		if r := recover(); r != nil {
			logErr("Recovered from handleGet panic! ", nil)
		}
	}()

	if mongoSession == nil {
		logErr("Invalid MongoDB session!", nil)
		fmt.Fprint(w, "{\"Error\":\"Failed while running query against database!\"}")
		return
	}

	// Clone the session
	session := *mongoSession.Copy()
	defer session.Close()

	// Run query against mongoDB
	collectionName := q.Get("c")
	collection := session.DB(config.MongoDatabase).C(collectionName)
	query := q.Get("q")

	var filter interface {}
	result := make([]map[string]interface{}, 0)

	if len(query) <= 2 {
		filter = bson.M {}
	} else {
		if err := bson.UnmarshalJSON([]byte(query), &filter); err != nil {
			logErr("JSON->BSON marshal error", err)
			fmt.Fprint(w, "{\"Error\":\"Failed while unmarshalling query!\"}")
			return
		}
	}

	if err := collection.Pipe(filter).All(&result); err != nil {
		logErr("Failed while running query against database!", err)
		fmt.Fprint(w, "{\"Error\":\"Failed while running query against database!\"}")
		return
	}

	// Serialise result to response stream
	r, err := json.Marshal(result)

	if err != nil {
		logErr("Failed while marshalling query results!", err)
		fmt.Fprint(w, "{\"Error\":\"Failed while marshalling query results!\"}")
		return
	}

	fmt.Fprint(w, string(r))
}

// Handles put requests
// Format : http://host:port/?a=put&c=collectionName&q={"key":"value"}
func handlePut(w http.ResponseWriter, q url.Values) {
	defer func() {
		if r := recover(); r != nil {
			logErr("Recovered from handlePut panic! ", nil)
		}
	}()

	if mongoSession == nil {
		logErr("Invalid MongoDB session!", nil)
		fmt.Fprint(w, "{\"Error\":\"Failed to add document!\"}")
		return
	}

	session := *mongoSession.Copy()
	defer session.Close()

	collectionName := q.Get("c")
	jsonDocument := q.Get("q")

	if len(jsonDocument) <= 0 {
		logErr("Invalid document specified!", nil)
		fmt.Fprint(w, "{\"Error\":\"Not adding blank documents to the collection! ERROR: Invalid document specified!\"}")
		return
	}

	var document interface {}

	if err := bson.UnmarshalJSON([]byte(jsonDocument), &document); err != nil {
		logErr("Failed to marshal document to interface!", err)
		fmt.Fprint(w, "{\"Error\":\"Failed to marshal json document to interface!\"}")
		return
	}

	if err := session.DB(config.MongoDatabase).C(collectionName).Insert(&document); err != nil {
		logErr("Failed to add document " +jsonDocument+ " to collection " + collectionName + "!", err)
		fmt.Fprint(w, "{\"Error\":\"Failed to add document " +jsonDocument+ " to collection " + collectionName + "!\"}")
		return
	}

	fmt.Fprint(w, "{\"Success\":\"Document added to collection\"}")
}
